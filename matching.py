# -*- coding: utf-8 -*-
"""
Routines for the actual matching code
"""
__license__ = "GPLv3"
__author__ = "Robert Scott"

from django.db import connection , transaction

_normalizing_abbreviations = (
	( "saint" , "st" ) ,
	( "road" , "rd" ) ,
	( "street" , "st" ) ,
	( "close" , "cl" ) ,
	( "lane" , "ln" ) ,
	( "mead" , "md" ) ,
	( "way" , "w" ) ,
	( "drive" , "dr" ) ,
	( "avenue" , "av" ) ,
	( "court" , "ct" ) ,
	( "square" , "sq" ) ,
	( "place" , "pl" ) ,
	( "crescent" , "cr" ) ,
	( "walk" , "wk" ) ,
	( "gardens" , "gdns" ) ,
	( "grove" , "gr" ) ,
	( "yard" , "yd" ) ,
	( "hill" , "h" ) ,
	( "terrace" , "ter" ) ,
	( "mews" , "mw" ) ,
)

_normalizing_character_substitutions = (
	( u"àáâãäå" , "a" ) ,
	( u"èéêë" , "e" ) ,
	( u"ìíîï" , "i" ) ,
	( u"ðòóôõö" , "o" ) ,
	( u"ùúûü" , "u" ) ,
	( u"ýŷỳ" , "y" ) ,
	( u"ŵẁẃẅ" , "w" ) ,
)

def generate_normalized_names ( table_name , namefieldname = "name" , targetfieldname_short = "normalized_name_short" , targetfieldname_long = "normalized_name_long" , normalizing_abbreviations = _normalizing_abbreviations , normalizing_character_substitutions = _normalizing_character_substitutions , modified_way_table_name = "" ):
	"""
	Generate normalized names in SQL
	"""
	cursor = connection.cursor ()
	
	if modified_way_table_name:
		# the table and column name flexibility doesn't really work when using these
		extra = """FROM %(modified_way_table_name)s WHERE %(table_name)s.id = %(modified_way_table_name)s.osmway_id AND %(namefieldname)s != ''""" % {
			"modified_way_table_name" : modified_way_table_name ,
			"table_name" : table_name ,
			"namefieldname" : namefieldname ,
		}
	else:
		extra = "WHERE %s != ''" % namefieldname
	
	# do single-character cleaning
	s = "lower ( %s )" % namefieldname
	for fromchars , tochar in normalizing_character_substitutions:
		s = r"regexp_replace ( %s , E'[%s]' , '%s' , 'g' )" % ( s , fromchars , tochar )
	
	# do whitespace cleaning & removal of remaining unwanted characters
	s = "regexp_replace ( trim ( from regexp_replace ( %s , '[^a-z0-9 ]' , '' , 'g' ) ) , ' {2,}' , ' ' , 'g' )" % s
	
	# go a further step and do abbreviations for the short name
	r = s
	for fromstring , tostring in normalizing_abbreviations:
		r = r"regexp_replace ( %s , E'\\y%s\\y' , '%s' , 'g' )" % ( r , fromstring , tostring )
	
	cursor.execute ( "UPDATE %s SET %s = %s , %s = %s %s;" % ( table_name , targetfieldname_short , r , targetfieldname_long , s , extra ) )
	
	transaction.set_dirty ()

def match ( principal_table , candidate_table , principal_geometry_field , candidate_geometry_field , target_table_name , radius = 300 ):
	cursor = connection.cursor ()

	# fill in the various table & field names
	query_string = """CREATE TEMPORARY TABLE %(target_table_name)s AS
		SELECT DISTINCT ON ( principal_id )
			principal_id ,
			candidate_id ,
			ldist
		FROM
		(
			SELECT
				* ,
				rank () OVER ( PARTITION BY candidate_id ORDER BY ldist ) AS candidate_rank
			FROM
			(
				SELECT
					* ,
					rank () OVER ( PARTITION BY principal_id ORDER BY ldist ) AS principal_rank
				FROM
				(
					SELECT
						%(principal_table)s.id AS principal_id ,
						%(candidate_table)s.id AS candidate_id ,
						levenshtein_oslmc ( %(principal_table)s.normalized_name_short ,
							%(principal_table)s.normalized_name_long ,
							%(candidate_table)s.normalized_name_short ,
							%(candidate_table)s.normalized_name_long ,
							%(candidate_table)s.normalized_name_en_short ,
							%(candidate_table)s.normalized_name_en_long ,
							%(candidate_table)s.normalized_name_cy_short ,
							%(candidate_table)s.normalized_name_cy_long ,
							%(candidate_table)s.normalized_name_gd_short ,
							%(candidate_table)s.normalized_name_gd_long ,
							%(candidate_table)s.normalized_alt_name_short ,
							%(candidate_table)s.normalized_alt_name_long
						) AS ldist ,
						ST_Distance ( %(principal_table)s.%(principal_geometry_field)s , %(candidate_table)s.%(candidate_geometry_field)s ) AS sdist ,
						%(principal_table)s.ref = %(candidate_table)s.ref AS equal_refs
					FROM
							%(principal_table)s
						JOIN
							%(candidate_table)s
						ON
						(
								%(candidate_table)s.%(candidate_geometry_field)s
							&&
								ST_Expand ( %(principal_table)s.%(principal_geometry_field)s , %%(radius)s )
						)
					WHERE
					(
							(
								%(principal_table)s.name != ''
							AND
								(
									%(candidate_table)s.name != ''
								OR
									%(candidate_table)s.name_en != ''
								OR
									%(candidate_table)s.name_cy != ''
								OR
									%(candidate_table)s.name_gd != ''
								OR
									%(candidate_table)s.alt_name != ''
								)
							)
						OR
							(
								%(principal_table)s.name = ''
							AND
								%(candidate_table)s.ref = %(principal_table)s.ref
							AND NOT
								(
									%(candidate_table)s.name != ''
								OR
									%(candidate_table)s.name_en != ''
								OR
									%(candidate_table)s.name_cy != ''
								OR
									%(candidate_table)s.name_gd != ''
								OR
									%(candidate_table)s.alt_name != ''
								)
							)
					)
				) AS s
			) AS t
		) AS u
		WHERE
			candidate_rank = 1 AND principal_rank = 1
		ORDER BY
			principal_id ,
			equal_refs DESC ,
			sdist ,
			candidate_id
	""" % {
		"principal_table" : principal_table ,
		"candidate_table" : candidate_table ,
		"principal_geometry_field" : principal_geometry_field ,
		"candidate_geometry_field" : candidate_geometry_field ,
		"target_table_name" : target_table_name ,
	}

	cursor.execute ( query_string , { "radius" : radius } )
	
	transaction.set_dirty ()
