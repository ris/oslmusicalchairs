# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

from os.path import abspath , splitext

class Migration(migrations.Migration):

    dependencies = [
        ('oslmusicalchairs', '0001_initial'),
    ]

    operations = [
        migrations.RunSQL(
            open ( splitext ( abspath ( __file__ ) )[0] + ".match.sql" , "rU" ).read ().decode ( "utf8" ).replace ( "%" , "%%" ),
        ),
    ]
