ALTER TABLE oslmusicalchairs_oslstreet ALTER normalized_name_short SET DEFAULT '';
ALTER TABLE oslmusicalchairs_oslstreet ALTER normalized_name_long SET DEFAULT '';
CREATE INDEX oslmusicalchairs_oslstreet_pseudorandom_index ON oslmusicalchairs_oslstreet ( ( md5 ( id::text ) ) );

CREATE OR REPLACE FUNCTION latest_match_pub_date ( street oslmusicalchairs_oslstreet ) RETURNS timestamp with time zone AS $$
BEGIN
	RETURN
	(
		SELECT
			pub_date
		FROM
			oslmusicalchairs_match ,
			oslmusicalchairs_matchset
		WHERE
				oslmusicalchairs_match.osl_id = street.id
			AND
				oslmusicalchairs_match.set_id = oslmusicalchairs_matchset.id
		ORDER BY pub_date DESC
		LIMIT 1
	);
END;
$$ LANGUAGE plpgsql IMMUTABLE;
-- naughtiness:     ^ it's not actually immutable

CREATE INDEX oslmusicalchairs_oslstreet_latest_match_pub_date_index ON oslmusicalchairs_oslstreet ( ( latest_match_pub_date ( oslmusicalchairs_oslstreet.* ) ) DESC , ( md5 ( id::text ) ) );

CREATE OR REPLACE FUNCTION latest_relevant_match_pub_date ( street oslmusicalchairs_oslstreet ) RETURNS timestamp with time zone AS $$
BEGIN
	RETURN
	(
		SELECT
			min ( pub_date )
		FROM
			oslmusicalchairs_match ,
			oslmusicalchairs_matchset
		WHERE
				oslmusicalchairs_match.osl_id = street.id
			AND
				oslmusicalchairs_match.set_id = oslmusicalchairs_matchset.id
		GROUP BY
			osmway_name , osmway_name_en , osmway_name_cy , osmway_name_gd , osmway_alt_name , osmway_not_name , osmway_ref
		ORDER BY min ( pub_date ) DESC
		LIMIT 1
	);
END;
$$ LANGUAGE plpgsql IMMUTABLE;
-- naughtiness:     ^ it's not actually immutable

CREATE INDEX oslmusicalchairs_oslstreet_latest_relevant_match_pub_date_index ON oslmusicalchairs_oslstreet ( ( latest_relevant_match_pub_date ( oslmusicalchairs_oslstreet.* ) ) DESC , ( md5 ( id::text ) ) );

CREATE OR REPLACE FUNCTION latest_match_not_name_match ( street oslmusicalchairs_oslstreet ) RETURNS boolean AS $$
BEGIN
	RETURN
	(
		SELECT
			( ( osmway_not_name != '' ) AND ( lower ( osmway_not_name ) = lower ( street.name ) ) ) AS not_name_match
		FROM
			oslmusicalchairs_match ,
			oslmusicalchairs_matchset
		WHERE
				oslmusicalchairs_match.osl_id = street.id
			AND
				oslmusicalchairs_match.set_id = oslmusicalchairs_matchset.id
		ORDER BY pub_date DESC
		LIMIT 1
	);
END;
$$ LANGUAGE plpgsql IMMUTABLE;
-- naughtiness:     ^ it's not actually immutable

CREATE INDEX oslmusicalchairs_oslstreet_latest_match_not_name_match_index ON oslmusicalchairs_oslstreet ( ( latest_match_not_name_match ( oslmusicalchairs_oslstreet.* ) ) );

CREATE OR REPLACE FUNCTION latest_match_ldist ( street oslmusicalchairs_oslstreet ) RETURNS float AS $$
BEGIN
	RETURN
	(
		SELECT
			ldist
		FROM
			oslmusicalchairs_match ,
			oslmusicalchairs_matchset
		WHERE
				oslmusicalchairs_match.osl_id = street.id
			AND
				oslmusicalchairs_match.set_id = oslmusicalchairs_matchset.id
		ORDER BY pub_date DESC
		LIMIT 1
	);
END;
$$ LANGUAGE plpgsql IMMUTABLE;
-- naughtiness:     ^ it's not actually immutable

CREATE INDEX oslmusicalchairs_oslstreet_latest_match_ldist_index ON oslmusicalchairs_oslstreet ( ( latest_match_ldist ( oslmusicalchairs_oslstreet.* ) = 0 ) NULLS FIRST , ( latest_match_not_name_match ( oslmusicalchairs_oslstreet.* ) ) , ( latest_match_ldist ( oslmusicalchairs_oslstreet.* ) ) DESC NULLS FIRST , ( name = '' ) , ( md5 ( id::text ) ) );

CREATE OR REPLACE FUNCTION latest_match_refs_match ( street oslmusicalchairs_oslstreet ) RETURNS boolean AS $$
BEGIN
	RETURN
	(
		SELECT
			( ( street.ref = '' ) OR ( lower ( osmway_ref ) = lower ( street.ref ) ) ) AS refs_match
		FROM
			oslmusicalchairs_match ,
			oslmusicalchairs_matchset
		WHERE
				oslmusicalchairs_match.osl_id = street.id
			AND
				oslmusicalchairs_match.set_id = oslmusicalchairs_matchset.id
		ORDER BY pub_date DESC
		LIMIT 1
	);
END;
$$ LANGUAGE plpgsql IMMUTABLE;
-- naughtiness:     ^ it's not actually immutable

CREATE INDEX oslmusicalchairs_oslstreet_latest_match_refs_match_index ON oslmusicalchairs_oslstreet ( ( latest_match_refs_match ( oslmusicalchairs_oslstreet.* ) ) );
