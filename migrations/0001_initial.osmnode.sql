-- the aim of this function is to attempt to update an entry with the specified id, but if it doesn't exist
-- insert it, without having to do another round trip to python to ask it to
CREATE OR REPLACE FUNCTION osmnode_update_or_insert ( new_point geometry , new_version integer , node_id bigint ) RETURNS void AS $$
BEGIN
	UPDATE oslmusicalchairs_osmnode SET point = new_point , version = new_version WHERE id = node_id;

	IF NOT FOUND THEN
		INSERT INTO oslmusicalchairs_osmnode ( point , version , id ) VALUES ( new_point , new_version , node_id );
	END IF;

	RETURN;
END;
$$ LANGUAGE plpgsql;
