CREATE OR REPLACE FUNCTION new_match_is_relevant ( old_match oslmusicalchairs_match , new_match oslmusicalchairs_match ) RETURNS boolean AS $$
  SELECT ( ($1 IS NULL) != ($2 IS NULL) ) OR $1.osmway_name != $2.osmway_name OR $1.osmway_name_en != $2.osmway_name_en OR $1.osmway_name_cy != $2.osmway_name_cy OR $1.osmway_name_gd != $2.osmway_name_gd OR $1.osmway_alt_name != $2.osmway_alt_name OR $1.osmway_not_name != $2.osmway_not_name OR $1.osmway_ref != $2.osmway_ref;
$$ LANGUAGE sql IMMUTABLE;
