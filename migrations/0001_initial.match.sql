CREATE OR REPLACE FUNCTION update_oslstreet_index_afterinsert () RETURNS TRIGGER AS $$
BEGIN
	UPDATE oslmusicalchairs_oslstreet SET id = id WHERE id = NEW.osl_id;
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION update_oslstreet_index_afterupdate () RETURNS TRIGGER AS $$
BEGIN
	UPDATE oslmusicalchairs_oslstreet SET id = id WHERE id = NEW.osl_id;
	UPDATE oslmusicalchairs_oslstreet SET id = id WHERE id = OLD.osl_id;
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION update_oslstreet_index_afterdelete () RETURNS TRIGGER AS $$
BEGIN
	UPDATE oslmusicalchairs_oslstreet SET id = id WHERE id = OLD.osl_id;
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER update_oslstreet_index_afterinsert AFTER INSERT ON oslmusicalchairs_match FOR EACH ROW EXECUTE PROCEDURE update_oslstreet_index_afterinsert ();
CREATE TRIGGER update_oslstreet_index_afterupdate AFTER UPDATE ON oslmusicalchairs_match FOR EACH ROW EXECUTE PROCEDURE update_oslstreet_index_afterupdate ();
CREATE TRIGGER update_oslstreet_index_afterdelete AFTER DELETE ON oslmusicalchairs_match FOR EACH ROW EXECUTE PROCEDURE update_oslstreet_index_afterdelete ();

-- this probably won't get inserted cleanly by django, but it belongs in this file so it's staying here. Will probably have to be inserted manually.
CREATE OR REPLACE FUNCTION previous_match ( match oslmusicalchairs_match ) RETURNS oslmusicalchairs_match AS $$
	SELECT
			oslmusicalchairs_match.*
		FROM
			oslmusicalchairs_match , oslmusicalchairs_matchset
		WHERE
				$1.osl_id = oslmusicalchairs_match.osl_id
			AND
				oslmusicalchairs_match.set_id = oslmusicalchairs_matchset.id
			AND
				oslmusicalchairs_matchset.pub_date < ( SELECT pub_date FROM oslmusicalchairs_matchset WHERE id = $1.set_id )
		ORDER BY
			oslmusicalchairs_matchset.pub_date DESC
	LIMIT 1;
$$ LANGUAGE sql IMMUTABLE;
-- naughtiness:     ^ it's not actually immutable

-- this probably won't get inserted cleanly by django, but it belongs in this file so it's staying here. Will probably have to be inserted manually.
CREATE OR REPLACE FUNCTION id_from_match ( match oslmusicalchairs_match ) RETURNS integer AS $$
	SELECT $1.id;
$$ LANGUAGE sql IMMUTABLE;
-- naughtiness:     ^ it's not actually immutable

-- this probably won't get inserted cleanly by django, but it belongs in this file so it's staying here. Will probably have to be inserted manually.
CREATE OR REPLACE FUNCTION new_match_is_relevant ( old_match oslmusicalchairs_match , new_match oslmusicalchairs_match ) RETURNS boolean AS $$
	SELECT ( $1 IS NULL != $2 IS NULL ) OR $1.osmway_name != $2.osmway_name OR $1.osmway_name_en != $2.osmway_name_en OR $1.osmway_name_cy != $2.osmway_name_cy OR $1.osmway_name_gd != $2.osmway_name_gd OR $1.osmway_alt_name != $2.osmway_alt_name OR $1.osmway_not_name != $2.osmway_not_name OR $1.osmway_ref != $2.osmway_ref;
$$ LANGUAGE sql IMMUTABLE;

CREATE INDEX oslmusicalchairs_match_match_is_relevant_index ON oslmusicalchairs_match ( ( new_match_is_relevant ( previous_match ( oslmusicalchairs_match.* ) , oslmusicalchairs_match.* ) ) );

-- this probably won't get inserted cleanly by django, but it belongs in this file so it's staying here. Will probably have to be inserted manually.
CREATE OR REPLACE FUNCTION new_match_is_relevant_downgrade ( old_match oslmusicalchairs_match , new_match oslmusicalchairs_match ) RETURNS boolean AS $$
	SELECT new_match_is_relevant ( $1 , $2 ) AND ( $2.ldist > $1.ldist OR ( $2.ldist IS NULL AND $1.ldist IS NOT NULL ) OR ( $1.id IS NULL AND $2.ldist != 0 ) )
$$ LANGUAGE sql IMMUTABLE;

CREATE INDEX oslmusicalchairs_match_match_is_relevant_downgrade_index ON oslmusicalchairs_match ( ( new_match_is_relevant_downgrade ( previous_match ( oslmusicalchairs_match.* ) , oslmusicalchairs_match.* ) ) );

-- this probably won't get inserted cleanly by django, but it belongs in this file so it's staying here. Will probably have to be inserted manually.
--
-- meant to just be used occasionally to re-cluster match table
CREATE OR REPLACE FUNCTION oslmusicalchairs_oslstreet_geohash ( oslstreet_id integer ) RETURNS text AS $$
	SELECT ST_GeoHash ( ST_StartPoint ( bbox_diagonal_4326 ) , 20 ) FROM oslmusicalchairs_oslstreet WHERE id = $1;
$$ LANGUAGE sql IMMUTABLE;
-- naughtiness:     ^ it's not actually immutable
