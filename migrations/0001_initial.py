# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import django.contrib.gis.db.models.fields

from os.path import abspath , splitext


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Match',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('osmway_id', models.BigIntegerField(null=True, blank=True)),
                ('osmway_version', models.PositiveIntegerField(null=True, blank=True)),
                ('osmway_name', models.TextField()),
                ('osmway_ref', models.TextField()),
                ('osmway_name_en', models.TextField()),
                ('osmway_name_cy', models.TextField()),
                ('osmway_name_gd', models.TextField()),
                ('osmway_alt_name', models.TextField()),
                ('osmway_not_name', models.TextField()),
                ('osmway_note', models.TextField()),
                ('osmway_notes', models.TextField()),
                ('ldist', models.FloatField(null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MatchSet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pub_date', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OSLStreet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.TextField(db_index=True, blank=True)),
                ('ref', models.TextField(db_index=True, blank=True)),
                ('bbox_diagonal_4326', django.contrib.gis.db.models.fields.LineStringField(srid=4326, null=True)),
                ('bbox_diagonal_27700', django.contrib.gis.db.models.fields.LineStringField(srid=27700)),
                ('settlement', models.TextField(blank=True)),
                ('locality', models.TextField(blank=True)),
                ('cou_unit', models.TextField(blank=True)),
                ('local_authority', models.TextField(blank=True)),
                ('tile_10k', models.TextField(blank=True)),
                ('tile_25k', models.TextField(blank=True)),
                ('source', models.TextField(blank=True)),
                ('normalized_name_short', models.TextField(blank=True)),
                ('normalized_name_long', models.TextField(blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OSMMembership',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('osmway_id', models.BigIntegerField(db_index=True)),
                ('osmnode_id', models.BigIntegerField(db_index=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OSMNode',
            fields=[
                ('id', models.BigIntegerField(serialize=False, primary_key=True)),
                ('point', django.contrib.gis.db.models.fields.PointField(srid=4326, spatial_index=False)),
                ('version', models.PositiveIntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OSMPlanetUpdate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('datetime', models.DateTimeField(default=datetime.datetime.now)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OSMWay',
            fields=[
                ('id', models.BigIntegerField(serialize=False, primary_key=True)),
                ('name', models.TextField(blank=True)),
                ('ref', models.TextField(blank=True)),
                ('version', models.PositiveIntegerField()),
                ('name_en', models.TextField(blank=True)),
                ('name_cy', models.TextField(blank=True)),
                ('name_gd', models.TextField(blank=True)),
                ('alt_name', models.TextField(blank=True)),
                ('not_name', models.TextField(blank=True)),
                ('note', models.TextField(blank=True)),
                ('notes', models.TextField(blank=True)),
                ('bbox_diagonal_4326', django.contrib.gis.db.models.fields.LineStringField(srid=4326, null=True, spatial_index=False)),
                ('bbox_diagonal_27700', django.contrib.gis.db.models.fields.LineStringField(srid=27700, null=True)),
                ('normalized_name_short', models.TextField(blank=True)),
                ('normalized_name_long', models.TextField(blank=True)),
                ('normalized_name_en_short', models.TextField(blank=True)),
                ('normalized_name_en_long', models.TextField(blank=True)),
                ('normalized_name_cy_short', models.TextField(blank=True)),
                ('normalized_name_cy_long', models.TextField(blank=True)),
                ('normalized_name_gd_short', models.TextField(blank=True)),
                ('normalized_name_gd_long', models.TextField(blank=True)),
                ('normalized_alt_name_short', models.TextField(blank=True)),
                ('normalized_alt_name_long', models.TextField(blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='matchset',
            name='osm_planet_update',
            field=models.ForeignKey(to='oslmusicalchairs.OSMPlanetUpdate'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='match',
            name='osl',
            field=models.ForeignKey(to='oslmusicalchairs.OSLStreet', db_constraint=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='match',
            name='set',
            field=models.ForeignKey(to='oslmusicalchairs.MatchSet'),
            preserve_default=True,
        ),
        migrations.RunSQL(
            open ( splitext ( abspath ( __file__ ) )[0] + ".match.sql" , "rU" ).read ().decode ( "utf8" ).replace ( "%" , "%%" ),
        ),
        migrations.RunSQL(
            open ( splitext ( abspath ( __file__ ) )[0] + ".oslstreet.sql" , "rU" ).read ().decode ( "utf8" ).replace ( "%" , "%%" ),
        ),
        migrations.RunSQL(
            open ( splitext ( abspath ( __file__ ) )[0] + ".osmnode.sql" , "rU" ).read ().decode ( "utf8" ).replace ( "%" , "%%" ),
        ),
        migrations.RunSQL(
            open ( splitext ( abspath ( __file__ ) )[0] + ".osmway.sql" , "rU" ).read ().decode ( "utf8" ).replace ( "%" , "%%" ),
        ),
    ]
