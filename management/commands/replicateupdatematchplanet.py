# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from django.db import transaction
from django.conf import settings

import os.path
import subprocess
from shutil import copyfile
from os import unlink , getpgid , getpid
import re
from datetime import datetime
from pytz import utc

from oslmusicalchairs.models import OSMPlanetUpdate

import updateosmplanet , matchstreets

class ReplicationError ( Exception ): pass

def _replicate_update ():
	"""
	We slightly hijack osmosis' planetdir to use for our own state information too
	"""
	
	osmosis_state_filename = os.path.join ( settings.REPLICATION_PLANET_DIR , "state.txt" )
	oslmc_state_filename = os.path.join ( settings.REPLICATION_PLANET_DIR , "oslmc_state.txt" )
	temp_changes_filename = os.path.join ( settings.REPLICATION_PLANET_DIR , "changes.osc.gz" )
	
	# only go ahead if the oslmc_state.txt is the same as osmosis' state.txt
	try:
		if ( open ( osmosis_state_filename , "rt" ).read () != open ( oslmc_state_filename , "rt" ).read () ):
			raise ReplicationError ( "State of oslmc database does not match osmosis' state. Fix this manually." )
	except IOError:
		# oslmc state file probably hasn't been created yet which is ok.
		pass
	
	replicate_args = [
		"osmosis" ,
		"--rri" ,
		"workingDirectory=%s" % settings.REPLICATION_PLANET_DIR ,
		"--simplify-change" ,
		"--wxc" ,
		temp_changes_filename ,
	]
	
	osmosis_popen = subprocess.Popen ( replicate_args )
	osmosis_popen.communicate ()
	
	if osmosis_popen.returncode:
		raise ReplicationError ( "Osmosis returned code %i. Not continuing." % osmosis_popen.returncode )
	
	tsmatch = re.search ( r"^timestamp=(.+?)Z$" , open ( osmosis_state_filename , "rt" ).read () , flags=re.MULTILINE )
	planet_timestamp = datetime.strptime ( tsmatch.group ( 1 ) , "%Y-%m-%dT%H\\:%M\\:%S" )
	update = OSMPlanetUpdate ( datetime = planet_timestamp.replace ( tzinfo = utc ) )
	update.save ()
	
	updateosmplanet.operation ( temp_changes_filename )
	
	unlink ( temp_changes_filename )

@transaction.atomic
def _replicate_update_commit ():
	_replicate_update ()
	print "Committing"

class Command ( BaseCommand ):
	help = "Replicates latest planet and produces a new matchset against it."
	args = "replicateupdatematchplanet"
	
	def handle ( self , *filenames , **options ):
		pidfile_filename = os.path.join ( settings.REPLICATION_PLANET_DIR , "oslmc.pidfile" ) 
		
		try:
			pidfile = open ( pidfile_filename , "rt" )
		except IOError:
			# file doesn't exist. that's good.
			pass
		else:
			try:
				# if no such pid exists, IOError will be raised and we will continue.
				existingpid = int ( pidfile.read () )
				getpgid ( existingpid )
				raise EnvironmentError ( "It looks like this procedure is already running at pid %i" % existingpid )
			except OSError:
				pass
		
			pidfile.close ()
		
		pidfile = open ( pidfile_filename , "wt" )
		pidfile.write ( str ( os.getpid () ) )
		pidfile.close ()
		
		_replicate_update_commit ()
		
		# set our oslmc_state.txt to the state that is now in the db
		copyfile ( os.path.join ( settings.REPLICATION_PLANET_DIR , "state.txt" ) , os.path.join ( settings.REPLICATION_PLANET_DIR , "oslmc_state.txt" ) )
		
		matchstreets.operation_commit ()
		
		unlink ( pidfile_filename )
		
		return 0
