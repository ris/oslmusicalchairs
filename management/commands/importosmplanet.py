# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from django.db import transaction

from oslmusicalchairs.matching import generate_normalized_names
from oslmusicalchairs.planet import expat_parse , planet_generator , calculate_bboxes
from oslmusicalchairs.tools import open_possibly_zipped_xml_file , transform_geometry_column , cheeky_get_raw_connection
from oslmusicalchairs.models import OSMPlanetUpdate

class Command ( BaseCommand ):
	help = "Imports an OSM planet file into the database"
	args = "importosmplanet [osm_planet_file.bz2...]"
	
	@transaction.atomic ()
	def handle ( self , *filenames , **options ):
		connection = cheeky_get_raw_connection ()
		cursor = connection.cursor ()
		
		# do this early so the time is as close as possible to the planet generation. TODO take a manual timestamp?
		update = OSMPlanetUpdate ()
		update.save ()
		
		planetfile = open_possibly_zipped_xml_file ( filenames[0] )
		
		print "Parsing planet"
		expat_parse ( planetfile , planet_generator () )
		
		print "Analyzing"
		cursor.execute ( "ANALYZE oslmusicalchairs_osmway ; ANALYZE oslmusicalchairs_osmnode ; ANALYZE oslmusicalchairs_osmmembership ;" )
		
		print "Calculating bboxes"
		calculate_bboxes ()
		
		print "Transforming"
		transform_geometry_column ( "oslmusicalchairs_osmway" , "bbox_diagonal_4326" , "bbox_diagonal_27700" , 27700 )
		
		print "Generating normalized names"
		generate_normalized_names ( "oslmusicalchairs_osmway" , "name" , "normalized_name_short" , "normalized_name_long" )
		generate_normalized_names ( "oslmusicalchairs_osmway" , "name_en" , "normalized_name_en_short" , "normalized_name_en_long" )
		generate_normalized_names ( "oslmusicalchairs_osmway" , "name_cy" , "normalized_name_cy_short" , "normalized_name_cy_long" )
		generate_normalized_names ( "oslmusicalchairs_osmway" , "name_gd" , "normalized_name_gd_short" , "normalized_name_gd_long" )
		generate_normalized_names ( "oslmusicalchairs_osmway" , "alt_name" , "normalized_alt_name_short" , "normalized_alt_name_long" )
		
		print "Analyzing"
		cursor.execute ( "ANALYZE oslmusicalchairs_osmway ; ANALYZE oslmusicalchairs_osmnode ; ANALYZE oslmusicalchairs_osmmembership ;" )
		
		print "Committing"
