# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from django.db import transaction

import csv
from sys import stdout

from oslmusicalchairs.tools import cheeky_get_raw_connection

class Command ( BaseCommand ):
	help = "Outputs an oslstreet table in a locator-v2-like format"
	
	def handle ( self , *filenames , **options ):
		connection = cheeky_get_raw_connection ()
		cursor = connection.cursor ()
	
		csv.register_dialect ( "oslocator" , delimiter = ":" , quoting = csv.QUOTE_NONE )
		writer = csv.writer ( stdout , "oslocator" )
		
		cursor.execute ( """SELECT
			name ,
			ref , 
			ST_X ( ST_Centroid ( bbox_diagonal_27700 ) )::integer ,
			ST_Y ( ST_Centroid ( bbox_diagonal_27700 ) )::integer ,
			ST_X ( ST_PointN ( bbox_diagonal_27700 , 1 ) )::integer ,
			ST_X ( ST_PointN ( bbox_diagonal_27700 , 2 ) )::integer ,
			ST_Y ( ST_PointN ( bbox_diagonal_27700 , 1 ) )::integer ,
			ST_Y ( ST_PointN ( bbox_diagonal_27700 , 2 ) )::integer ,
			settlement ,
			locality ,
			cou_unit ,
			local_authority ,
			tile_10k ,
			tile_25k ,
			source ,
			id
		FROM
			oslmusicalchairs_oslstreet 
		ORDER BY id ;""" )
		
		for row in cursor:
			writer.writerow ( (
				row[0].encode ( "latin-1" ) ,
				row[1].encode ( "latin-1" ) ,
				row[2] ,
				row[3] ,
				row[4] ,
				row[5] ,
				row[6] ,
				row[7] ,
				row[8].encode ( "latin-1" ) ,
				row[9].encode ( "latin-1" ) ,
				row[10].encode ( "latin-1" ) ,
				row[11].encode ( "latin-1" ) ,
				row[12].encode ( "latin-1" ) ,
				row[13].encode ( "latin-1" ) ,
				row[14].encode ( "latin-1" ) ,
				row[15] ,
				)
			)
