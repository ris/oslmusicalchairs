# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from django.db import transaction , connection

from sys import stderr
from optparse import make_option

from oslmusicalchairs.models import *

class Command ( BaseCommand ):
	help = "Re-clusters some important tables"
	
	option_list = BaseCommand.option_list + (
		make_option ( "--skip-oslocator" , action = "store_true" , dest = "skip_oslocator" , default = False , help = "Skip clustering OS Locator table." ) ,
		make_option ( "--skip-osm" , action = "store_true" , dest = "skip_osm" , default = False , help = "Skip clustering OSM tables." ) ,
		make_option ( "--skip-matches" , action = "store_true" , dest = "skip_matches" , default = False , help = "Skip clustering match tables." ) ,
	)
	
	def handle ( self , **options ):
		tables = {
			"oslmusicalchairs_oslstreet" : ( "( ST_GeoHash ( ST_StartPoint ( bbox_diagonal_4326 ) , 20 ) )" , "skip_oslocator" ) ,
			"oslmusicalchairs_osmway" : ( "( ST_GeoHash ( ST_StartPoint ( bbox_diagonal_4326 ) , 20 ) )" , "skip_osm" ) ,
			"oslmusicalchairs_osmnode" : ( "( ST_GeoHash ( point , 20 ) )" , "skip_osm" ) ,
			"oslmusicalchairs_match" : ( "( oslmusicalchairs_oslstreet_geohash ( osl_id ) )" , "skip_matches" ) ,
		}
		
		for table , ( indexdef , skip_key ) in tables.iteritems ():
			if options.get ( skip_key ):
				continue
			
			with transaction.atomic ():
				cursor = connection.cursor ()
				print >> stderr , "Creating index for table %s" % table
				cursor.execute ( "CREATE INDEX tmp ON %s ( %s )" % ( table , indexdef ) )
				print >> stderr , "Clustering table %s (beware exclusive lock!)" % table
				cursor.execute ( "CLUSTER %s USING tmp" % table )
				print >> stderr , "Dropping index for table %s" % table
				cursor.execute ( "DROP INDEX tmp" )
