# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from django.db import transaction
from django.contrib.gis.geos import GEOSGeometry

import csv
from sys import stdout

from oslmusicalchairs.tools import cheeky_get_raw_connection

class Command ( BaseCommand ):
	help = "Outputs a CSV of each OSLStreet and its latest match"
	
	def handle ( self , *filenames , **options ):
		connection = cheeky_get_raw_connection ()
		cursor = connection.cursor ()
	
		writer = csv.writer ( stdout )
		
		cursor.execute ( """SELECT DISTINCT ON ( oslmusicalchairs_oslstreet.id )
			oslmusicalchairs_oslstreet.id ,
			oslmusicalchairs_oslstreet.name ,
			oslmusicalchairs_oslstreet.ref ,
			ST_Transform ( ST_Centroid ( oslmusicalchairs_oslstreet.bbox_diagonal_27700 ) , 4326 ) ,
			oslmusicalchairs_match.ldist,
			oslmusicalchairs_match.osmway_id ,
			oslmusicalchairs_match.osmway_version ,
			oslmusicalchairs_match.osmway_name ,
			oslmusicalchairs_match.osmway_ref
		FROM
			oslmusicalchairs_oslstreet ,
			oslmusicalchairs_match ,
			oslmusicalchairs_matchset
		WHERE
				oslmusicalchairs_match.osl_id = oslmusicalchairs_oslstreet.id
			AND
				oslmusicalchairs_match.set_id = oslmusicalchairs_matchset.id
		ORDER BY oslmusicalchairs_oslstreet.id , oslmusicalchairs_matchset.pub_date DESC ;""" )
		
		for row in cursor:
			point_tuple = GEOSGeometry ( row[3] ).get_coords ()
			writer.writerow ( (
				row[0] ,
				row[1].encode ( "utf-8" , "ignore" ) ,
				row[2].encode ( "utf-8" , "ignore" ) ,
				point_tuple[0] ,
				point_tuple[1] ,
				row[4] ,
				row[5] ,
				row[6] ,
				row[7].encode ( "utf-8" , "ignore" ) ,
				row[8].encode ( "utf-8" , "ignore" ) ,
				)
			)
