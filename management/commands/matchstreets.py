# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from django.db import transaction , connection
from django.conf import settings

from datetime import datetime

from oslmusicalchairs.matching import match

def operation ():
	cursor = connection.cursor ()
	
	print "Matching"
	match ( "oslmusicalchairs_oslstreet" , "oslmusicalchairs_osmway" , "bbox_diagonal_27700" , "bbox_diagonal_27700" , "_match_results" , settings.OSLMC_MATCH_RADIUS )
	
	print "Inserting new matchset"
	cursor.execute ( "INSERT INTO oslmusicalchairs_matchset ( pub_date , osm_planet_update_id ) VALUES ( %s , ( SELECT id FROM oslmusicalchairs_osmplanetupdate ORDER BY datetime DESC LIMIT 1 ) ) RETURNING id;" , ( datetime.now () ,) )

	( matchset_id , ) = cursor.fetchone ()
	
	print "Merging results"
	cursor.execute ( """INSERT INTO
		oslmusicalchairs_match ( set_id , osl_id , osmway_id , osmway_version , osmway_name , osmway_ref , osmway_name_en , osmway_name_cy , osmway_name_gd , osmway_alt_name , osmway_not_name , osmway_note , osmway_notes , ldist )
	SELECT
		%s AS set_id ,
		oslstreet_id ,
		generatedmatches.candidate_id ,
		generatedmatches.version ,
		COALESCE ( generatedmatches.osmway_name , '' ) ,
		COALESCE ( generatedmatches.osmway_ref , '' ) ,
		COALESCE ( generatedmatches.name_en , '' ) ,
		COALESCE ( generatedmatches.name_cy , '' ) ,
		COALESCE ( generatedmatches.name_gd , '' ) ,
		COALESCE ( generatedmatches.alt_name , '' ) ,
		COALESCE ( generatedmatches.not_name , '' ) ,
		COALESCE ( generatedmatches.note , '' ) ,
		COALESCE ( generatedmatches.notes , '' ) ,
		generatedmatches.ldist * 0.5
	FROM
		(
				oslmusicalchairs_oslstreet AS generatedmatches_oslstreet ( oslstreet_id )
			LEFT OUTER JOIN
			(
					_match_results
				JOIN
					oslmusicalchairs_osmway AS generatedmatches_osmway ( id , osmway_name , osmway_ref )
				ON
				(
					candidate_id = generatedmatches_osmway.id
				)
			)
			ON
			(
				oslstreet_id = principal_id
			)
		) AS generatedmatches
		LEFT OUTER JOIN
		(
			SELECT DISTINCT ON ( oslmusicalchairs_match.osl_id )
				*
			FROM
				oslmusicalchairs_match ,
				oslmusicalchairs_matchset
			WHERE
				oslmusicalchairs_match.set_id = oslmusicalchairs_matchset.id
			ORDER BY
				oslmusicalchairs_match.osl_id ,
				oslmusicalchairs_matchset.pub_date DESC
		) AS latestmatches
		ON
		(
				generatedmatches.oslstreet_id = latestmatches.osl_id
			AND
				(
						(
								generatedmatches.candidate_id = latestmatches.osmway_id
							AND
								generatedmatches.version = latestmatches.osmway_version
							AND
								generatedmatches.ldist * 0.5 = latestmatches.ldist
						)
					OR
						(
								generatedmatches.candidate_id IS NULL
							AND
								latestmatches.osmway_id IS NULL
						)
				)
		)
	WHERE
		latestmatches.osl_id IS NULL;""" , ( matchset_id ,) )

@transaction.atomic
def operation_commit ():
	operation ()
	print "Committing"

class Command ( BaseCommand ):
	help = "Compares OSL and OSM streets, producing a new MatchSet"
	args = "matchstreets"
	
	def handle ( self , *filenames , **options ):
		operation_commit ()
