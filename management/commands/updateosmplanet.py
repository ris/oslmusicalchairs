# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from django.db import transaction

from oslmusicalchairs.matching import generate_normalized_names
from oslmusicalchairs.planet import expat_parse , planet_change_generator , calculate_bboxes , purge_nodeless_ways
from oslmusicalchairs.tools import open_possibly_zipped_xml_file , transform_geometry_column , cheeky_get_raw_connection
from oslmusicalchairs.models import OSMPlanetUpdate

def operation ( filename ):
	connection = cheeky_get_raw_connection ()
	cursor = connection.cursor ()
	
	changefile = open_possibly_zipped_xml_file ( filename )
	
	print "Parsing osc"
	expat_parse ( changefile , planet_change_generator ( modified_node_table_name = "_modified_nodes" , modified_way_table_name = "_modified_ways" ) )
	
	#print "Analyzing"
	#cursor.execute ( "ANALYZE oslmusicalchairs_osmway ; ANALYZE oslmusicalchairs_osmnode ; ANALYZE oslmusicalchairs_osmmembership ;" )
	
	print "Calculating bboxes"
	calculate_bboxes ( modified_node_table_name = "_modified_nodes" , modified_way_table_name = "_modified_ways" )
	
	print "Purging ways with no remaining nodes"
	purge_nodeless_ways ( "oslmusicalchairs_osmway" , "oslmusicalchairs_osmmembership" , "bbox_diagonal_4326" )
	
	print "Transforming"
	transform_geometry_column ( "oslmusicalchairs_osmway" , "bbox_diagonal_4326" , "bbox_diagonal_27700" , 27700 , modified_node_table_name = "_modified_nodes" , modified_way_table_name = "_modified_ways" )
	
	print "Generating normalized names"
	generate_normalized_names ( "oslmusicalchairs_osmway" , "name" , "normalized_name_short" , "normalized_name_long" , modified_way_table_name = "_modified_ways" )
	generate_normalized_names ( "oslmusicalchairs_osmway" , "name_en" , "normalized_name_en_short" , "normalized_name_en_long" , modified_way_table_name = "_modified_ways" )
	generate_normalized_names ( "oslmusicalchairs_osmway" , "name_cy" , "normalized_name_cy_short" , "normalized_name_cy_long" , modified_way_table_name = "_modified_ways" )
	generate_normalized_names ( "oslmusicalchairs_osmway" , "name_gd" , "normalized_name_gd_short" , "normalized_name_gd_long" , modified_way_table_name = "_modified_ways" )
	generate_normalized_names ( "oslmusicalchairs_osmway" , "alt_name" , "normalized_alt_name_short" , "normalized_alt_name_long" , modified_way_table_name = "_modified_ways" )
	
	#print "Analyzing"
	#cursor.execute ( "ANALYZE oslmusicalchairs_osmway ; ANALYZE oslmusicalchairs_osmnode ; ANALYZE oslmusicalchairs_osmmembership ;" )

class Command ( BaseCommand ):
	help = "Updates the OSM planet tables from an osm changefile"
	args = "updateosmplanet [osm_change_file.osc.gz...]"
	
	@transaction.atomic
	def handle ( self , *filenames , **options ):
		# do this early so the time is as close as possible to the changefile generation. TODO perhaps read datetime from osmosis state file.
		update = OSMPlanetUpdate ()
		update.save ()
		
		operation ( filename[0] )
		
		print "Committing"
