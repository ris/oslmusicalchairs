# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from django.db import transaction

import csv
from optparse import make_option

from oslmusicalchairs.matching import generate_normalized_names
from oslmusicalchairs.tools import cheeky_get_raw_connection , open_possibly_zipped_xml_file , transform_geometry_column

from oslmusicalchairs.models import OSMPlanetUpdate

class Command ( BaseCommand ):
	help = "Imports an OS Locator pseudo-csv file as supplied by OS"
	args = "[filename...]"
	
	option_list = BaseCommand.option_list + (
		make_option ( "--use-ids" , action = "store_true" , dest = "use_ids" , default = False , help = "Use ids supplied in final extra column." ) ,
		make_option ( "-f" , "--format-version" , type = "int" , dest = "format_version" , default = 2 , help = "OS Locator file format version." ) ,
	)
	
	format_version_columns = {
		1 : {
			"settlement" : 9 ,
			"locality" : 10 ,
			"cou_unit" : 11 ,
			"local_authority" : 12 ,
			"tile_10k" : 13 ,
			"tile_25k" : 14 ,
			"source" : 15 ,
			"id" : 16 ,
		} ,
		2 : {
			"settlement" : 8 ,
			"locality" : 9 ,
			"cou_unit" : 10 ,
			"local_authority" : 11 ,
			"tile_10k" : 12 ,
			"tile_25k" : 13 ,
			"source" : 14 ,
			"id" : 15 ,
		} ,
	}
	
	@transaction.atomic ()
	def handle ( self , *filenames , **options ):
		connection = cheeky_get_raw_connection ()
		cursor = connection.cursor ()
	
		f = open_possibly_zipped_xml_file ( filenames[0] )
	
		csv.register_dialect ( "oslocator" , delimiter = ":" , quoting = csv.QUOTE_NONE )
		reader = csv.reader ( f , "oslocator" )
		
		if not options.get("use_ids"):
			# we've got to restart this sequence so we're certain the assigned ids are right
			print "Restarting oslstreet id seq"
			cursor.execute ( "ALTER SEQUENCE oslmusicalchairs_oslstreet_id_seq RESTART ;" )
		
		insert_text = "INSERT INTO oslmusicalchairs_oslstreet ( name , ref , bbox_diagonal_27700 , settlement , locality , cou_unit , local_authority , tile_10k , tile_25k , source %s ) VALUES ( %%s , %%s , ST_SetSRID ( ST_MakeLine ( ST_Point ( %%s , %%s ) , ST_Point ( %%s , %%s ) ) , 27700 ) , %%s , %%s , %%s , %%s , %%s , %%s , %%s %s ) ;" % ( ( ", id" , ", %s" ) if options.get("use_ids") else ( "" , "" ) )
		
		print "Inserting data"
		for row in reader:
			cursor.execute (
				insert_text ,
				(
					unicode ( row[0] , "latin1" ) ,
					unicode ( row[1] , "latin1" ) ,
					int ( row[4] ) , int ( row[6] ) , int ( row[5] ) , int ( row[7] ) ,
					unicode ( row[ self.format_version_columns[options.get("format_version")]["settlement"] ] , "latin1" ) ,
					unicode ( row[ self.format_version_columns[options.get("format_version")]["locality"] ] , "latin1" ) ,
					unicode ( row[ self.format_version_columns[options.get("format_version")]["cou_unit"] ] , "latin1" ) ,
					unicode ( row[ self.format_version_columns[options.get("format_version")]["local_authority"] ] , "latin1" ) ,
					unicode ( row[ self.format_version_columns[options.get("format_version")]["tile_10k"] ] , "latin1" ) ,
					unicode ( row[ self.format_version_columns[options.get("format_version")]["tile_25k"] ] , "latin1" ) ,
					unicode ( row[ self.format_version_columns[options.get("format_version")]["source"] ] , "latin1" ) ,
				) + ( (int ( row[ self.format_version_columns[options.get("format_version")]["id"] ] ),) if options.get("use_ids") else () )
			)
		
		print "Analyzing"
		cursor.execute ( "ANALYZE oslmusicalchairs_oslstreet" );
		
		print "Transforming"
		transform_geometry_column ( "oslmusicalchairs_oslstreet" , "bbox_diagonal_27700" , "bbox_diagonal_4326" , 4326 )
		
		print "Generating normalized names"
		generate_normalized_names ( "oslmusicalchairs_oslstreet" , "name" , "normalized_name_short" , "normalized_name_long" )
		
		print "Analyzing"
		cursor.execute ( "ANALYZE oslmusicalchairs_oslstreet" );
		
		print "Committing"
