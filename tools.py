# -*- coding: utf-8 -*-

from django.db import connection , transaction

def coroutine ( func ):
	def start ( *args , **kwargs ):
		cr = func ( *args , **kwargs )
		cr.next ()
		return cr
	return start

def cheeky_get_raw_connection ():
	"""
	Does something very cheeky and quite naughty. Uses some tricks to get the raw (hopefully psycopg2) connection object so we can use it with
	all its features and none of the overhead of django's connection, whilst still automatically getting all django's db settings.
	Yet another thing that destroys our db independence and will make us vulnerable to django internal changes.
	"""
	
	return connection.cursor().cursor.connection

def transform_geometry_column ( table_name , source_column_name , target_column_name , srid , modified_node_table_name = "" , modified_way_table_name = "" ):
	cursor = connection.cursor ()
	
	if modified_node_table_name and modified_way_table_name:
		# the table and column name flexibility doesn't really work when using these
		extra = """
		FROM
			(
				SELECT
					osmway_id
				FROM
						%(modified_node_table_name)s
					JOIN
						oslmusicalchairs_osmmembership
					USING ( osmnode_id )
			UNION
				SELECT
					osmway_id
				FROM
					%(modified_way_table_name)s
			) AS all_modified_nodes
		WHERE %(table_name)s.id = all_modified_nodes.osmway_id""" % {
			"modified_node_table_name" : modified_node_table_name ,
			"modified_way_table_name" : modified_way_table_name ,
			"table_name" : table_name ,
		}
	else:
		extra = ""
	
	querystring = "UPDATE %(table_name)s SET %(target_column_name)s = ST_Transform ( %(source_column_name)s , %%s ) %(extra)s;" % {
		"table_name" : table_name ,
		"target_column_name" : target_column_name ,
		"source_column_name" : source_column_name ,
		"extra" : extra ,
	}
	
	cursor.execute ( querystring , ( srid ,) )
	transaction.set_dirty ()

def open_possibly_zipped_xml_file ( filename ):
	if filename.endswith ( ".bz2" ):
		from bz2 import BZ2File
		return BZ2File ( filename , mode = "rb" , buffering = (1<<16) )
	elif filename.endswith ( ".gz" ):
		from gzip import GzipFile
		return GzipFile ( filename , mode = "rb" )
	else:
		return open ( filename , mode = "r" )

def import_osl_file ( f , connection ):
	"""
	Imports the OS Locator file as supplied by OS into the django model's table. connection is a dbapi (psycopg2) connection object.
	Don't use this through django - it would seem there is a nasty memory leak somewhere with django 1.2 and long db operations.
	This is postgis specific, but should be replaceable with a cutomized version for another db.
	"""
	import csv
	
	csv.register_dialect ( "oslocator" , delimiter = ":" , quoting = csv.QUOTE_NONE )
	reader = csv.reader ( f , "oslocator" )
	
	cursor = connection.cursor ()
	
	for row in reader:
		cursor.execute (
			"INSERT INTO oslmusicalchairs_oslstreet ( name , ref , centroid , bbox , u0 , u1 , u2 , u3 , u4 , u5 , u6 , u7 ) VALUES ( %s , %s , ST_Transform ( ST_PointFromText ( %s , 27700 ) , 4326 ) , ST_Transform ( ST_GeomFromText ( %s , 27700 ) , 4326 ) , %s , %s , %s , %s , %s , %s , %s , %s );" ,
			(
				unicode ( row[0] , "latin1" ) ,
				unicode ( row[1] , "latin1" ) ,
				"POINT(%s %s)" % ( row[2] , row[3] ) ,
				"LINESTRING(%s %s,%s %s)" % ( row[4] , row[6] , row[5] , row[7] ) ,
				unicode ( row[8] , "latin1" ) ,
				unicode ( row[9] , "latin1" ) ,
				unicode ( row[10] , "latin1" ) ,
				unicode ( row[11] , "latin1" ) ,
				unicode ( row[12] , "latin1" ) ,
				unicode ( row[13] , "latin1" ) ,
				unicode ( row[14] , "latin1" ) ,
				unicode ( row[15] , "latin1" )
			)
		)
		
	connection.commit ()

def import_matches ( f , connection , planet_date = None ):
	"""
	import a new matchset from a csv. connection should be a dbapi connection object.
	"""
	from datetime import datetime
	
	cursor = connection.cursor ()
	
	cursor.execute ( "INSERT INTO oslmusicalchairs_matchset ( pub_date , planet_date ) VALUES ( %s , %s ) RETURNING id;" , ( datetime.now () , planet_date ) )
	
	( matchset_id , ) = cursor.fetchone ()
	
	cursor.execute ( """CREATE TEMPORARY TABLE csv_repr (
		osl_id integer NOT NULL ,
		osm_id bigint ,
		osm_name text ,
		osm_ref text ,
		ldist integer
	);""" )
	
	cursor.copy_expert ( "COPY csv_repr FROM STDIN CSV FORCE NOT NULL osm_name , osm_ref;" , f )
	
	cursor.execute ( """INSERT INTO
		oslmusicalchairs_match ( set_id , osl_id , osm_id , osm_name , osm_ref , ldist )
	SELECT
		%s AS set_id ,
		csv_repr.osl_id ,
		csv_repr.osm_id ,
		csv_repr.osm_name ,
		csv_repr.osm_ref ,
		csv_repr.ldist
	FROM
			csv_repr
		LEFT OUTER JOIN
		(
			SELECT DISTINCT ON ( oslmusicalchairs_match.osl_id )
				*
			FROM
				oslmusicalchairs_match,
				oslmusicalchairs_matchset
			WHERE
				oslmusicalchairs_match.set_id = oslmusicalchairs_matchset.id
			ORDER BY
				oslmusicalchairs_match.osl_id ,
				oslmusicalchairs_matchset.pub_date DESC
		) AS latestmatches
		ON
		(
				csv_repr.osl_id = latestmatches.osl_id
			AND
				(
						(
								csv_repr.osm_id = latestmatches.osm_id
							AND
								csv_repr.osm_name = latestmatches.osm_name
							AND
								csv_repr.osm_ref = latestmatches.osm_ref
							AND
								csv_repr.ldist = latestmatches.ldist
						)
					OR
						(
								csv_repr.osm_id IS NULL
							AND
								latestmatches.osm_id IS NULL
						)
				)
		)
	WHERE
		latestmatches.osl_id IS NULL;""" , ( matchset_id ,) )
	
	connection.commit ()
