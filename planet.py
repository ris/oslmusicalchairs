# -*- coding: utf-8 -*-

from django.db import transaction
from django.conf import settings

from tools import cheeky_get_raw_connection , coroutine

_event_start = 0
_event_end = 1
_event_commit = 2

def _gen_inner ( osm_node_buffer_size = 128 , modified_node_table_name = "" , modified_way_table_name = "" ):
	"""
	inspired by david beazley's "curious course on coroutines and concurrency"
	"""
	connection = cheeky_get_raw_connection ()
	cursor = connection.cursor ()
	
	try:
		bbox_floats = (
			float ( settings.PLANET_BBOX[0] ) ,
			float ( settings.PLANET_BBOX[1] ) ,
			float ( settings.PLANET_BBOX[2] ) ,
			float ( settings.PLANET_BBOX[3] ) ,
		)
	except AttributeError:
		bbox_floats = None
	
	while True:
		node_buffer = []
		event , value = (yield)
		
		if event == _event_start and value[0] in ( "create" , "modify" , "delete" , "osm" ):
			# an actioncontext of "osm" is an initial planet import
			actioncontext = value[0]
			
			while True:
				event , value = (yield)
				if len ( node_buffer ) >= osm_node_buffer_size or ( node_buffer and not ( event == _event_start and value[0] == "node" ) ):
					# if we've got nodes in the buffer and we start an element that's not a node, act on the buffer.
					# or if the buffer's reached its limit, do the same.
					if actioncontext == "delete":
						cursor.executemany ( "DELETE FROM oslmusicalchairs_osmnode WHERE id = %s;" , node_buffer )
					elif actioncontext == "modify":
						cursor.executemany ( "SELECT osmnode_update_or_insert ( ST_SetSRID ( ST_MakePoint ( %s , %s ) , 4326 ) , %s , %s );" , node_buffer )
					else:
						cursor.executemany ( "INSERT INTO oslmusicalchairs_osmnode ( point , version , id ) VALUES ( ST_SetSRID ( ST_MakePoint ( %s , %s ) , 4326 ) , %s , %s );" , node_buffer )
					
					if modified_node_table_name and actioncontext in ( "create" , "modify" ):
						cursor.executemany ( "INSERT INTO %s ( osmnode_id ) VALUES ( %%s );" % modified_node_table_name , [ ( n[3] ,) for n in node_buffer ] )
					# clear node buffer
					node_buffer = []
				
				if event == _event_start and value[0] == "node":
					# nodes tend to come in long runs so we can save time by building them up in a buffer
					# and acting on them in batches
					
					floatlon = float ( value[1]["lon"] )
					floatlat = float ( value[1]["lat"] )
					# check this node interests us by being in the PLANET_BBOX. if not, dont waste time on it
					if ( bbox_floats is None ) or (
						floatlon >= bbox_floats[0] and
						floatlat >= bbox_floats[1] and
						floatlon <= bbox_floats[2] and
						floatlat <= bbox_floats[3] ):
						if actioncontext == "delete":
							node_buffer.append ( ( value[1]["id"] ,) )
						else:
							node_buffer.append ( ( value[1]["lon"] , value[1]["lat"] , value[1]["version"] , value[1]["id"] ) )
					
					# skip through uninteresting node inner elements
					while True:
						event , value = (yield)
						if event == _event_end and value == "node":
							break
				elif event == _event_start and value[0] == "way":
					way_id = value[1]["id"]
					way_version = value[1]["version"]
					way_name = ""
					way_ref = ""
					way_name_en = ""
					way_name_cy = ""
					way_name_gd = ""
					way_alt_name = ""
					way_not_name = ""
					way_note = ""
					way_notes = ""
					is_highway = False
					member_buffer = []
					# run through way inner elements
					while True:
						event , value = (yield)
						if event == _event_start and value[0] == "nd":
							member_buffer.append ( ( value[1]["ref"] , way_id ) )
						elif event == _event_start and value[0] == "tag":
							if value[1]["k"] == "name":
								way_name = value[1]["v"]
							elif value[1]["k"] == "ref":
								way_ref = value[1]["v"]
							elif value[1]["k"] == "name:en":
								way_name_en = value[1]["v"]
							elif value[1]["k"] == "name:cy":
								way_name_cy = value[1]["v"]
							elif value[1]["k"] == "name:gd":
								way_name_gd = value[1]["v"]
							elif value[1]["k"] == "alt_name":
								way_alt_name = value[1]["v"]
							elif value[1]["k"] == "not:name":
								way_not_name = value[1]["v"]
							elif value[1]["k"] == "note":
								way_note = value[1]["v"]
							elif value[1]["k"] == "notes":
								way_notes = value[1]["v"]
							elif value[1]["k"] == "highway":
								is_highway = True
						elif event == _event_end and value == "way":
							break
					
					if actioncontext in ( "modify" , "delete" ):
						# for modify actions, what we'll do is attempt to delete it and any references to it and if it now interests us, reinsert it.
						cursor.execute ( "DELETE FROM oslmusicalchairs_osmmembership WHERE osmway_id = %s" , ( way_id ,) )
						cursor.execute ( "DELETE FROM oslmusicalchairs_osmway WHERE id = %s" , ( way_id ,) )
					
					if actioncontext in ( "osm" , "create" , "modify" ) and ( is_highway and ( way_name or way_ref or way_name_en or way_name_cy or way_name_gd or way_alt_name ) ):
						cursor.executemany ( "INSERT INTO oslmusicalchairs_osmmembership ( osmnode_id , osmway_id ) VALUES ( %s , %s )" , member_buffer )
						cursor.execute ( "INSERT INTO oslmusicalchairs_osmway ( id , name , ref , name_en , name_cy , name_gd , alt_name , not_name , note , notes , version ) VALUES ( %s , %s , %s , %s , %s , %s , %s , %s , %s , %s , %s )" , ( way_id , way_name , way_ref , way_name_en , way_name_cy , way_name_gd , way_alt_name , way_not_name , way_note , way_notes , way_version ) )
						if modified_way_table_name and actioncontext in ( "create" , "modify" ):
							cursor.execute ( "INSERT INTO %s ( osmway_id ) VALUES ( %%s );" % modified_way_table_name , ( way_id ,) )
				elif event == _event_end and value == actioncontext:
					break

planet_generator = coroutine ( _gen_inner )

@coroutine
def planet_change_generator ( osm_node_buffer_size = 128 , modified_node_table_name = "" , modified_way_table_name = "" ):
	connection = cheeky_get_raw_connection ()
	cursor = connection.cursor ()
	
	if modified_node_table_name:
		cursor.execute ( "CREATE TEMPORARY TABLE %s ( osmnode_id bigint NOT NULL );" % modified_node_table_name )
	if modified_way_table_name:
		cursor.execute ( "CREATE TEMPORARY TABLE %s ( osmway_id bigint NOT NULL );" % modified_way_table_name )
	
	return _gen_inner ( osm_node_buffer_size , modified_node_table_name , modified_way_table_name )

def expat_parse ( f , gen ):
	from xml.parsers import expat
	
	p = expat.ParserCreate ()
	try:
		p.buffer_size = (1<<16)
	except AttributeError:
		# probably not python 2.6 - nothing to worry about
		pass
	p.buffer_text = True
	p.StartElementHandler = lambda name , attrs : gen.send ( ( _event_start , ( name , attrs ) ) )
	p.EndElementHandler = lambda name : gen.send ( ( _event_end , name ) )
	p.ParseFile ( f )
	
	transaction.set_dirty ()

def calculate_bboxes ( modified_node_table_name = "" , modified_way_table_name = "" ):
	from django.db import connection
	cursor = connection.cursor ()

	if modified_node_table_name and modified_way_table_name:
		extrafrom = """,
		(
			SELECT
				osmway_id
			FROM
					%(modified_node_table_name)s
				JOIN
					oslmusicalchairs_osmmembership
				USING ( osmnode_id )
		UNION
			SELECT
				osmway_id
			FROM
				%(modified_way_table_name)s
		) AS all_modified_nodes""" % {
			"modified_node_table_name" : modified_node_table_name ,
			"modified_way_table_name" : modified_way_table_name ,
		}
		
		extrawhere = "AND outer_membership.osmway_id = all_modified_nodes.osmway_id"
	else:
		extrafrom = extrawhere = ""
	
	querystring = """UPDATE oslmusicalchairs_osmway SET bbox_diagonal_4326 =
	ST_SetSRID (
			ST_MakeLine (
				ST_Point ( min_x , min_y ) ,
				ST_Point ( max_x , max_y )
			) ,
			4326
		)
	FROM
	(
		SELECT DISTINCT
			outer_membership.osmway_id ,
			min ( ST_X ( point ) ) AS min_x ,
			max ( ST_X ( point ) ) AS max_x ,
			min ( ST_Y ( point ) ) AS min_y ,
			max ( ST_Y ( point ) ) AS max_y
		FROM
			oslmusicalchairs_osmnode ,
			oslmusicalchairs_osmmembership AS outer_membership %s
		WHERE
			outer_membership.osmnode_id = oslmusicalchairs_osmnode.id %s
		GROUP BY
			outer_membership.osmway_id
	) AS s
	WHERE
		s.osmway_id = oslmusicalchairs_osmway.id;""" % ( extrafrom , extrawhere )
	
	cursor.execute ( querystring )
	
	transaction.set_dirty ()

def purge_nodeless_ways ( way_table_name , membership_table_name , geometry_column_name ):
	"""
	A way whose nodes are all outside our PLANET_BBOX will have had its bbox calculated as null. Discard.
	What's more, a way with no spatial presence is no use to our analysis.
	"""
	from django.db import connection
	
	cursor = connection.cursor ()
	
	cursor.execute ( "DELETE FROM %(way_table_name)s WHERE %(geometry_column_name)s ISNULL" % {
		"way_table_name" : way_table_name ,
		"geometry_column_name" : geometry_column_name ,
	} )
	
	cursor.execute ( "DELETE FROM %(membership_table_name)s WHERE NOT EXISTS ( SELECT 1 FROM %(way_table_name)s WHERE %(way_table_name)s.id = %(membership_table_name)s.osmway_id )" % {
		"way_table_name" : way_table_name ,
		"membership_table_name" : membership_table_name ,
	} )
	
	transaction.set_dirty ()
