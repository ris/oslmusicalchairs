# -*- coding: utf-8 -*-
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.gis.db import models
from django.conf import settings

import datetime
from uuid import UUID , uuid5
from hashlib import sha1

class OSMNode ( models.Model ):
	objects = models.GeoManager ()
	
	id = models.BigIntegerField ( primary_key = True )
	
	point = models.PointField ( srid = 4326  , spatial_index = False )
	version = models.PositiveIntegerField ()

class OSMWay ( models.Model ):
	objects = models.GeoManager ()
	
	id = models.BigIntegerField ( primary_key = True )
	
	name = models.TextField ( blank = True )
	ref = models.TextField ( blank = True )
	version = models.PositiveIntegerField ()
	
	name_en = models.TextField ( blank = True )
	name_cy = models.TextField ( blank = True )
	name_gd = models.TextField ( blank = True )
	alt_name = models.TextField ( blank = True )
	not_name = models.TextField ( blank = True )
	note = models.TextField ( blank = True )
	notes = models.TextField ( blank = True )
	
	bbox_diagonal_4326 = models.LineStringField ( srid = 4326 , spatial_index = False , null = True )
	bbox_diagonal_27700 = models.LineStringField ( srid = 27700 , null = True )
	
	normalized_name_short = models.TextField ( blank = True )
	normalized_name_long = models.TextField ( blank = True )
	normalized_name_en_short = models.TextField ( blank = True )
	normalized_name_en_long = models.TextField ( blank = True )
	normalized_name_cy_short = models.TextField ( blank = True )
	normalized_name_cy_long = models.TextField ( blank = True )
	normalized_name_gd_short = models.TextField ( blank = True )
	normalized_name_gd_long = models.TextField ( blank = True )
	normalized_alt_name_short = models.TextField ( blank = True )
	normalized_alt_name_long = models.TextField ( blank = True )

class OSMMembership ( models.Model ):
	"""
	We have to use a special model and integer fields rather than a straight manytomany field because we have to be able to cope with lossy situations. ie - ways with nodes we do not have an entry for because it was chopped out of the bounding box.
	
	Doing it this way allows us to skip the REFERENCES constraint.
	"""
	osmway_id = models.BigIntegerField ( db_index = True )
	osmnode_id = models.BigIntegerField ( db_index = True )

class OSMPlanetUpdate ( models.Model ):
	"""
	represents a specific revision of the planet imported or updated into the db
	"""
	datetime = models.DateTimeField ( default = datetime.datetime.now )


class OSLStreet ( models.Model ):
	objects = models.GeoManager ()
	
	name = models.TextField ( blank = True , db_index = True )
	ref = models.TextField ( blank = True , db_index = True )
	
	# There is no support for using actual bboxes in geodjango so i am storing the bbox as a line, bottom left -> top right
	bbox_diagonal_4326 = models.LineStringField ( srid = 4326 , null = True )
	bbox_diagonal_27700 = models.LineStringField ( srid = 27700 )
	
	settlement = models.TextField ( blank = True )
	locality = models.TextField ( blank = True )
	cou_unit = models.TextField ( blank = True )
	local_authority = models.TextField ( blank = True )
	tile_10k = models.TextField ( blank = True )
	tile_25k = models.TextField ( blank = True )
	source = models.TextField ( blank = True )
	
	normalized_name_short = models.TextField ( blank = True )
	normalized_name_long = models.TextField ( blank = True )
	
	def geometry_dict ( self ):
		return {
				"type" : "Polygon" ,
				"coordinates" : [ [ [ self.bbox_diagonal_4326[0][0] , self.bbox_diagonal_4326[0][1] ] , [ self.bbox_diagonal_4326[0][0] , self.bbox_diagonal_4326[1][1] ] , [ self.bbox_diagonal_4326[1][0] , self.bbox_diagonal_4326[1][1] ] , [ self.bbox_diagonal_4326[1][0] , self.bbox_diagonal_4326[0][1] ] , [ self.bbox_diagonal_4326[0][0] , self.bbox_diagonal_4326[0][1] ] ] ] ,
		}
	
	def centroid_geometry_dict ( self ):
		centroid = self.bbox_diagonal_4326.centroid
		return {
				"type" : "Point" ,
				"coordinates" : [ centroid[0] , centroid[1] ] ,
		}
	
	def get_summary_dict ( self , authoritative = True ):
		# 'authoritative' is really a property of the collection of delivered features, but openlayers
		# doesn't support these, or at least just discards the properties of a FeatureCollection, so we
		# have to attach the property here.
		properties = { "has_name" : self.has_name if hasattr ( self , "has_name" ) else bool ( self.name ) }
		
		if hasattr ( self , "latest_match_ldist" ):
			if self.latest_match_ldist is not None:
				properties["match"] = {
					"ldist" : self.latest_match_ldist ,
					"refs_match" : self.latest_match_refs_match ,
					"not_name_match" : self.latest_match_not_name_match ,
				}
		else:
			# do it the long way round - not implemented yet
			pass
		
		return {
			"id" : self.id ,
			"type" : "Feature" ,
			"properties" : properties ,
			"geometry" : self.geometry_dict () if authoritative else self.centroid_geometry_dict () ,
		}
	
	def get_detail_dict ( self , geometry = False ):
		properties = {}
		for prop in ( "name" , "ref" , "settlement" , "locality" , "cou_unit" , "local_authority" , "tile_10k" , "tile_25k" , "source" ):
			properties[prop] = getattr ( self , prop )
		
		try:
			matches = self.match_set.order_by ( "-set__pub_date" )
			properties["matches"] = []
			for match in matches:
				m = {
					"matchset_pub_date" : str ( match.set.pub_date ) ,
					"matchset_planet_date" : str ( match.set.osm_planet_update.datetime ) ,
				}
				if match.osmway_id is not None:
					m["ldist"] = match.ldist
					m["osmway_id"] = match.osmway_id
					m["osmway_name"] = match.osmway_name
					m["osmway_ref"] = match.osmway_ref
					m["osmway_version"] = match.osmway_version
					m["refs_match"] = match.refs_match
					m["not_name_match"] = match.not_name_match
					if match.osmway_name_en:
						m["osmway_name_en"] = match.osmway_name_en
					if match.osmway_name_cy:
						m["osmway_name_cy"] = match.osmway_name_cy
					if match.osmway_name_gd:
						m["osmway_name_gd"] = match.osmway_name_gd
					if match.osmway_alt_name:
						m["osmway_alt_name"] = match.osmway_alt_name
					if match.osmway_not_name:
						m["osmway_not_name"] = match.osmway_not_name
					if match.osmway_note:
						m["osmway_note"] = match.osmway_note
					if match.osmway_notes:
						m["osmway_notes"] = match.osmway_notes
				properties["matches"].append ( m )
		except ObjectDoesNotExist:
			pass

		result = {
			"id" : self.id ,
			"type" : "Feature" ,
			"properties" : properties ,
		}
		
		if geometry:
			result["geometry"] = self.geometry_dict ()
		
		return result


class MatchSet ( models.Model ):
	pub_date = models.DateTimeField ( auto_now_add = True )
	osm_planet_update = models.ForeignKey ( OSMPlanetUpdate )

_namespace_uuid = UUID ( bytes = sha1 ( "oslmusicalchairs.match." + settings.SECRET_KEY ).digest ()[:16] )

class _PreviousMatchFetchingDescriptor ( object ):
	def __get__ ( self , instance , instance_type = None ):
		# loosely based on behaviour of SingleRelatedObjectDescriptor
		previous_match = Match.objects.get ( pk = instance.previous_match_id ) if instance.previous_match_id is not None else None
		# overwrite own reference on instance
		setattr ( instance , "previous_match" , previous_match )
		return previous_match

class Match ( models.Model ):
	objects = models.GeoManager ()
	
	set = models.ForeignKey ( MatchSet )
	
	# A constraint here is not wanted because we want to be able to refer to osl streets that don't exist anymore.
	osl = models.ForeignKey ( OSLStreet , db_index = True , db_constraint = False )
	
	# snapshots of the osmway's properties at time of matching.
	osmway_id = models.BigIntegerField ( blank = True , null = True )
	osmway_version = models.PositiveIntegerField ( blank = True , null = True )
	osmway_name = models.TextField ()
	osmway_ref = models.TextField ()
	
	osmway_name_en = models.TextField ()
	osmway_name_cy = models.TextField ()
	osmway_name_gd = models.TextField ()
	osmway_alt_name = models.TextField ()
	osmway_not_name = models.TextField ()
	osmway_note = models.TextField ()
	osmway_notes = models.TextField ()
	
	ldist = models.FloatField ( blank = True , null = True )
	
	
	previous_match = _PreviousMatchFetchingDescriptor ()
	
	@property
	def uuid ( self ):
		return uuid5 ( _namespace_uuid , str ( self.id ) )
	
	@property
	def refs_match ( self ):
		return ( not self.osl.ref ) or ( self.osl.ref.lower () == self.osmway_ref.lower () )
	
	@property
	def not_name_match ( self ):
		return self.osmway_not_name and ( self.osl.name.lower () == self.osmway_not_name.lower () )
	
	@property
	def description ( self ):
		# an "identical" javascript implementation of this exists in map_template.xhtml
		if self.ldist is not None:
			if self.ldist == 0:
				newtext = "Near perfect match."
			elif self.ldist < 1:
				newtext = "Probably just a spacing disagreement."
			elif self.ldist <= 2:
				newtext = "Probable match with small disagreement."
			elif self.ldist <= 4:
				newtext = "Possible match with major disagreement."
			else:
				newtext = "Unlikely match."

			if not self.refs_match:
				newtext += " Refs do not match."

			if self.not_name_match:
				newtext += " not:name implies Locator entry is wrong."

			return newtext
		else:
			if bool ( self.osl.name ):
				return "No match";
			else:
				return "No exact match: locator entry has no name";
