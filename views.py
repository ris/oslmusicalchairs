# -*- coding: utf-8 -*-
from django.views.decorators.http import require_POST , require_GET
from django.http import HttpResponseBadRequest , HttpResponse , HttpResponseNotFound , Http404
from django.contrib.gis.geos import LineString , Polygon
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.core.exceptions import *
from django.core.urlresolvers import reverse
from django.conf import settings

import json

from tools import coroutine

from models import *
from wfs import WFSException , wfs_exception_response

_event_start = 0
_event_end = 1
_event_character_data = 2

@require_POST
def mock_wfs ( request , ordermode ):
	"""
	serves up the data from OSLStreet in a very cut down WFS interface.
	
	in fact, this is probably the most standards-uncompliant WFS server in existance, which
	i take a perverse sort of pride in.
	"""
	from xml.parsers import expat
	
	result_limit = 1024
	
	@coroutine
	def wfspost_gen ( outdict ):
		while True:
			event , value = (yield)
			if event == _event_start and value[0] == "gml:Box":
				while True:
					event , value = (yield)
					if event == _event_start and value[0] == "gml:coordinates":
						while True:
							event , value = (yield)
							if event == _event_character_data:
								outdict["bbstring"] = value
							elif event == _event_end and value == "gml:Coordinates":
								break
					elif event == _event_end and value == "gml:Box":
						break
		
	orders = { # tuples of ( order string , reverse )
		"pseudorandom" : ( "md5 ( id::text )" , False ) ,
		"recentupdate" : ( "latest_match_pub_date ( oslmusicalchairs_oslstreet.* ) DESC , md5 ( id::text )" , True ) ,
		"recentrelevantupdate" : ( "latest_relevant_match_pub_date ( oslmusicalchairs_oslstreet.* ) DESC , md5 ( id::text )" , True ) ,
		"badmatches" : ( "latest_match_ldist ( oslmusicalchairs_oslstreet.* ) = 0 NULLS FIRST , latest_match_not_name_match ( oslmusicalchairs_oslstreet.* ) , latest_match_ldist ( oslmusicalchairs_oslstreet.* ) DESC NULLS FIRST , name = '' , md5 ( id::text )" , True ) ,
		"authoritativeonly" : None ,
	}
	
	if ordermode not in orders:
		return HttpResponseNotFound ( "No ordermode by that name found" )
	
	try:
		bbdict = {}
		gen = wfspost_gen ( bbdict )
		
		p = expat.ParserCreate ()
		p.StartElementHandler = lambda name , attrs : gen.send ( ( _event_start , ( name , attrs ) ) )
		p.EndElementHandler = lambda name : gen.send ( ( _event_start , name ) )
		p.CharacterDataHandler = lambda data : gen.send ( ( _event_character_data , data ) )
		p.Parse ( request.body )
		
		( xmin , ymin ) , ( xmax , ymax ) = [ [ float ( coord ) for coord in coordpair.split( "," ) ] for coordpair in bbdict["bbstring"].split ( " " ) ]
	
		# This is not used, but is a useful snippet so I'm leaving it here.
		#if Polygon ( ( ( xmin , ymin ) , ( xmin , ymax ) , ( xmax , ymax ) , ( xmax , ymin ) , ( xmin , ymin ) ) , srid = 4326 ).transform ( 27700 , clone = True ).area > 5.0e8:
			#raise WFSException ( "Area too big. Try zooming in." )
		
		if ordermode == "authoritativeonly" and OSLStreet.objects.filter ( bbox_diagonal_4326__bboverlaps = LineString ( ( ( xmin , ymin ) , ( xmax , ymax ) ) , srid = 4326 ) ).count () > result_limit:
			# This is a nasty way of signalling to OL that this view is (or would be) non-authoritative. In future should change this, but only when I get round to using some specific signalling for this at both ends.
			return HttpResponse ( """{ "type" : "Point" }""" , content_type = "application/json" )
		
		
		streetquery = """
			SELECT
				id ,
				bbox_diagonal_4326 ,
				latest_match_ldist ( oslmusicalchairs_oslstreet.* ) ,
				latest_match_refs_match ( oslmusicalchairs_oslstreet.* ) ,
				name != '' AS has_name ,
				latest_match_not_name_match ( oslmusicalchairs_oslstreet.* )
			FROM
				oslmusicalchairs_oslstreet
			WHERE
				bbox_diagonal_4326 && ST_SetSRID ( ST_MakeBox2D ( ST_Point ( %%s , %%s ) , ST_Point ( %%s , %%s ) ) , 4326 )
				%s
				%s
			LIMIT
				%%s;
		"""
		
		whereparamlist = [ xmin , ymin , xmax , ymax ]
		extrawhere = ""
		
		# Why am I using url query parameters and not CQL? Let me give you
		# a 160 page document on why not...
		for value in request.GET.getlist ( "name" ):
			extrawhere = " AND name = %s "
			whereparamlist.append ( value.upper () )
			
		for value in request.GET.getlist ( "name_inc" ):
			extrawhere = " AND name LIKE '%%' || %s || '%%' "
			whereparamlist.append ( value.upper () )
		
		for value in request.GET.getlist ( "ref" ):
			extrawhere = " AND ref = %s "
			whereparamlist.append ( value.upper () )
		
		## to do the actual query, we use raw sql.
		streets = OSLStreet.objects.raw ( streetquery % ( extrawhere , ( ( "ORDER BY %s" % orders[ordermode][0] ) if orders[ordermode] else "" ) ) , whereparamlist + [ result_limit+1 ] )
		
		streetlist = list ( streets )
		
		# ugh. I hate this but it's the simplest way of doing it.
		if orders[ordermode] and orders[ordermode][1]:
			streetlist.reverse ()
		
		authoritative = ( len ( streetlist ) <= result_limit )
		
		featurelist = [ street.get_summary_dict ( authoritative = authoritative ) for street in streetlist[:result_limit] ]
	except WFSException , e:
		return wfs_exception_response ( e )
	
	return HttpResponse ( json.dumps ( {
		"type" : "FeatureCollection" ,
		"features" : featurelist ,
	} ) , content_type = "application/json" )

_feed_endpoints = {
	"all" : ( None , "All new matches" ) ,
	"relevant" : ( "new_match_is_relevant ( previous_match ( oslmusicalchairs_match.* ) , oslmusicalchairs_match.* )" , "New relevant matches" ) ,
	"relevant_downgrades" : ( "new_match_is_relevant_downgrade ( previous_match ( oslmusicalchairs_match.* ) , oslmusicalchairs_match.* )" , "New match state downgrades" ) ,
}

@require_GET
def map ( request ):
	"""
	Serves the openlayers map.
	"""
	
	context = {
		"feed_endpoints" : { key : desc for key , ( where , desc ) in _feed_endpoints.iteritems () } ,
	}
	
	try:
		oslstreet = OSLStreet.objects.get ( id = int ( request.GET["osl_id"] ) )
		bblinestring = oslstreet.bbox_diagonal_4326
		context["osl_bounds_left"] = bblinestring[0][0]
		context["osl_bounds_bottom"] = bblinestring[0][1]
		context["osl_bounds_right"] = bblinestring[1][0]
		context["osl_bounds_top"] = bblinestring[1][1]
	except ( ValueError , KeyError , ObjectDoesNotExist ):
		pass
	
	context["planet_bounds_left"] = settings.PLANET_BBOX[0]
	context["planet_bounds_bottom"] = settings.PLANET_BBOX[1]
	context["planet_bounds_right"] = settings.PLANET_BBOX[2]
	context["planet_bounds_top"] = settings.PLANET_BBOX[3]
	
	return render_to_response ( "map_template.xhtml" , RequestContext ( request , context ) )

@require_GET
def oslstreet ( request , oslstreet_id ):
	try:
		id = int ( oslstreet_id )
		street = OSLStreet.objects.get ( id = id )
	except ValueError , ObjectDoesNotExist:
		return HttpResponseNotFound ()
	
	return HttpResponse ( json.dumps ( street.get_detail_dict ( geometry = True ) ) , content_type = "application/json" )

@require_GET
def feed ( request , endpoint_label ):
	try:
		endpoint_where , endpoint_desc = _feed_endpoints[endpoint_label]
	except KeyError:
		raise Http404
	
	# would like to order by >1 column, but sorting by multiple independent indexes is too slow
	matches = Match.objects.select_related ().order_by ( "-set__pub_date" ).extra ( {
		"previous_match_id" : "id_from_match ( previous_match ( oslmusicalchairs_match.* ) )" ,
	} )
	
	if endpoint_where is not None:
		matches = matches.extra ( where = [ endpoint_where ] )
	
	if "bbox" in request.GET:
		bbox_term_strings = request.GET["bbox"].split ( "," )
		
		try:
			bbox_left , bbox_bottom , bbox_right , bbox_top = tuple ( float ( term ) for term in bbox_term_strings )
		except ValueError:
			return HttpResponseBadRequest ( "Improperly formed bbox parameter" )
		
		if bbox_left > bbox_right or bbox_bottom > bbox_top:
			return HttpResponseBadRequest ( "bbox parameters should be left,bottom,right,top - submitted parameter doesn't make sense interpreted this way" )
		
		matches = matches.filter ( osl__bbox_diagonal_4326__bboverlaps = LineString ( ( ( bbox_left , bbox_bottom ) , ( bbox_right , bbox_top ) ) , srid = 4326 ) )
	
	context = {
		"matches" : matches[:50] ,
		"endpoint_desc" : endpoint_desc ,
		"map_abs_url" : request.build_absolute_uri ( reverse ( map ) ) ,
		# only going to make a vague attempt at a main link as bbox calculation -> zoom level is a pain
		"latest_matchset" : MatchSet.objects.order_by ( "-pub_date" )[0] ,
	}
	
	if "bbox" in request.GET:
		context.update ( {
			"bbox_joined" : "%f,%f,%f,%f" % ( bbox_left , bbox_bottom , bbox_right , bbox_top ) ,
			"center_lat" : "%f" % ( ( bbox_bottom + bbox_top ) / 2.0 ) ,
			"center_lon" : "%f" % ( ( bbox_left + bbox_right ) / 2.0 ) ,
		} )
	
	return render_to_response ( "feed_template.xml" , RequestContext ( request , context ) , content_type = "application/rss+xml" )
