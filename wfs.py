from django.http import HttpResponse

class WFSException ( Exception ) : pass

def wfs_exception_response ( e ):
	return HttpResponse ( """
<?xml version="1.0" ?>
<ExceptionReport version="1.1.0" xmlns="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="owsExceptionReport.xsd">
	<Exception>
		<ExceptionText>%s</ExceptionText>
	</Exception>
</ExceptionReport>
	""" % e.message , mimetype = "application/xml" )