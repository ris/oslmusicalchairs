# -*- coding: utf-8 -*-
from django.conf.urls import *
from django.views.generic import RedirectView

import views

urlpatterns = patterns('',
	(r"^mockwfs/([a-zA-Z_]*)$" , views.mock_wfs ) ,
	(r"^rss/([a-zA-Z_]+)$" , views.feed ) ,
	(r"^map$" , views.map ) ,
	(r"^oslstreet/(?P<oslstreet_id>[0-9]+)/?$" , views.oslstreet ) ,
	(r"^$" , RedirectView.as_view ( url = "map" ) ) ,
)
